import React, { Component } from 'react';
import { BrowserRouter, Route, Link } from 'react-router-dom';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  Container
 } from 'reactstrap';
import Articles from './Articles';
import Videos from './Videos';

class App extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  render = () =>
      <BrowserRouter>
        <div>
          <Navbar color="light" light expand="md">
            <NavbarBrand href="/">كشكول</NavbarBrand>
            <NavbarToggler onClick={this.toggle} />
            <Collapse isOpen={this.state.isOpen} navbar>
              <Nav className="ml-auto" navbar>
                <NavItem>
                  <Link to="/articles" className="nav-link">
                    مقالات عن الويب
                  </Link>
                </NavItem>
                <NavItem>
                  <Link to="/videos" className="nav-link">
                    فيديوهات عن الويب
                  </Link>
                </NavItem>
              </Nav>
            </Collapse>
          </Navbar>

          <Container style={{ marginTop: 50 }}>
            <Route path="/" exact component={Articles}/>
            <Route path="/articles" component={Articles}/>
            <Route path="/videos" component={Videos}/>
          </Container>
        </div>
      </BrowserRouter>
}

export default App;
