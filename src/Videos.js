import React, { Component } from 'react';
import { Card, CardBody, CardText } from 'reactstrap';

class Videos extends Component {
  render = () =>
    <div>
      <div style={{ textAlign: 'right', width: 560 }}>
        <Card>
          <iframe
            title="vidoe1"
            width="560"
            height="315"
            src="https://www.youtube.com/embed/iZRyrg0c6mM"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen/>
          <CardBody>
            <CardText>
              المصفوفات في لغة جافاسكريبت
            </CardText>
          </CardBody>
        </Card>
      </div>
      <div style={{ textAlign: 'right', width: 560, marginTop: 50 }}>
        <Card>
          <iframe
            title="video2"
            width="560"
            height="315"
            src="https://www.youtube.com/embed/X5sxlBurbSg"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen/>
          <CardBody>
            <CardText>
              العوامل في لغة جافاسكريبت
            </CardText>
          </CardBody>
        </Card>
      </div>
    </div>
}

export default Videos;
